import QtQuick
import QtQuick.Controls
import Krita.Utilities
import Krita.Widgets

Window {
    id: root
    width: 600
    height: 600
    visible: true
    title: "Template Project"

    function refreshPalette()
    {
        if (darkModeSwitch.checked) {
            KisUI.applyDarkPaletteToWindow(root);
        } else {
            KisUI.applyLightPaletteToWindow(root);
        }
    }

    Component.onCompleted: refreshPalette()

    Row {
        id: switchRow
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        padding: 10
        Label { text: "Dark Mode"; font.bold: true; anchors.verticalCenter: parent.verticalCenter; }
        Switch {
            id: darkModeSwitch
            onToggled: refreshPalette()
        }
    }

    ScrollView {
        anchors.top: switchRow.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        Item {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 10
            implicitHeight: col.implicitHeight + anchors.margins * 2

            Column {
                id: col
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 10

                Label { text: "KisGroupButton"; font.bold: true; }
                Row {
                    spacing: 10;
                    leftPadding: 20
                    Column {
                        spacing: 5
                        Label { text: "NoGroup" }
                        KisGroupButton {
                            groupPosition: KisGroupButton.GroupPosition.NoGroup
                        }
                    }
                    Column {
                        spacing: 5
                        Label { text: "Group" }
                        Row {
                            spacing: 5
                            KisGroupButton {
                                checkable: true
                                groupPosition: KisGroupButton.GroupPosition.GroupLeft
                                autoExclusive: true
                                checked: true
                            }
                            KisGroupButton {
                                checkable: true
                                groupPosition: KisGroupButton.GroupPosition.GroupCenter
                                autoExclusive: true
                            }
                            KisGroupButton {
                                checkable: true
                                groupPosition: KisGroupButton.GroupPosition.GroupRight
                                autoExclusive: true
                            }
                        }
                    }
                }

                Label { text: "KisOptionButtonStrip"; font.bold: true; }
                Row {
                    spacing: 10;
                    leftPadding: 20
                    Column {
                        spacing: 5
                        Label { text: "Enabled" }
                        KisOptionButtonStrip {
                            id: buttonStrip1
                            objectName: "buttonStrip1"
                            checkedButtonIndex: 1
                            KisGroupButton {
                                checkable: true
                                groupPosition: KisGroupButton.GroupPosition.GroupLeft
                            }
                            KisGroupButton {
                                checkable: true
                                groupPosition: KisGroupButton.GroupPosition.GroupCenter
                            }
                            KisGroupButton {
                                checkable: true
                                groupPosition: KisGroupButton.GroupPosition.GroupRight
                            }
                        }
                    }
                    Column {
                        spacing: 5
                        Label { text: "Disabled" }
                        KisOptionButtonStrip {
                            id: buttonStrip2
                            objectName: "buttonStrip2"
                            enabled: false
                            checkedButtonIndex: buttonStrip1.checkedButtonIndex
                            KisGroupButton {
                                checkable: true
                                groupPosition: KisGroupButton.GroupPosition.GroupLeft
                            }
                            KisGroupButton {
                                checkable: true
                                groupPosition: KisGroupButton.GroupPosition.GroupCenter
                            }
                            KisGroupButton {
                                checkable: true
                                groupPosition: KisGroupButton.GroupPosition.GroupRight
                            }
                            Component.onCompleted: buttonStrip1.checkedButtonIndexChanged.connect(
                                                        () => {buttonStrip2.checkedButtonIndex = buttonStrip1.checkedButtonIndex})
                        }
                    }
                }

                Label { text: "KisIntParseSpinBox"; font.bold: true; }
                Row {
                    spacing: 10;
                    leftPadding: 20
                    Column {
                        spacing: 5
                        Label { text: "Enabled" }
                        KisIntParseSpinBox {
                            id: sp1
                            prefix: "Width: "
                            suffix: "px"
                            value: 30
                        }
                    }
                    Column {
                        spacing: 5
                        Label { text: "Disabled" }
                        KisIntParseSpinBox {
                            prefix: "Width: "
                            suffix: "px"
                            value: 30
                            enabled: false
                        }
                    }
                }

                Label { text: "KisDoubleParseSpinBox"; font.bold: true; }
                Row {
                    spacing: 10
                    leftPadding: 20
                    Column {
                        spacing: 5
                        Label { text: "Enabled" }
                        KisDoubleParseSpinBox {
                            prefix: "Height: "
                            suffix: "px"
                            dFrom: -100
                            dValue: 60
                        }
                    }
                    Column {
                        spacing: 5
                        Label { text: "Disabled" }
                        KisDoubleParseSpinBox {
                            prefix: "Height: "
                            suffix: "px"
                            dFrom: -100
                            dValue: 60
                            enabled: false
                        }
                    }
                }

                Label { text: "KisIntSliderSpinBox"; font.bold: true; }
                Row {
                    spacing: 10
                    leftPadding: 20
                    Column {
                        spacing: 5
                        Label { text: "Enabled" }
                        KisIntSliderSpinBox {
                            prefix: "Width: "
                            suffix: "px"
                            value: 30
                        }
                    }
                    Column {
                        spacing: 5
                        Label { text: "Disabled" }
                        KisIntSliderSpinBox {
                            prefix: "Width: "
                            suffix: "px"
                            value: 30
                            enabled: false
                        }
                    }
                }

                Label { text: "KisDoubleSliderSpinBox"; font.bold: true; }
                Row {
                    spacing: 10
                    leftPadding: 20
                    Column {
                        spacing: 5
                        Label { text: "Enabled" }
                        KisDoubleSliderSpinBox {
                            id: doubleSliderSpinBox1
                            prefix: "A bit long prefix: "
                            suffix: "px"
                            softDFrom: 20
                            softDTo: 80
                            softRangeActive: true
                            dValue: 60
                            width: implicitWidth
                        }
                    }
                    Column {
                        spacing: 5
                        Label { text: "Disabled" }
                        KisDoubleSliderSpinBox {
                            id: doubleSliderSpinBox2
                            prefix: "A bit long prefix: "
                            suffix: "px"
                            softDFrom: 20
                            softDTo: 80
                            softRangeActive: true
                            dValue: doubleSliderSpinBox1.dValue
                            width: implicitWidth
                            enabled: false
                            Connections {
                                target: doubleSliderSpinBox1
                                function onDValueChanged() { doubleSliderSpinBox2.dValue = doubleSliderSpinBox1.dValue; }
                            }
                        }
                    }
                }

                Label { text: "KisAngleGauge"; font.bold: true; }
                Row {
                    spacing: 10
                    leftPadding: 20
                    Column {
                        spacing: 5
                        Label { text: "Enabled" }
                        KisAngleGauge {
                            id: angleGauge
                            Connections {
                                function onAngleChanged()
                                {
                                    let a = angleGauge.angle < 0 ? angleGauge.angle + 360 : angleGauge.angle;
                                    angleSelector1.angle = angleSelector2.angle = angleSelector3.angle =
                                    angleSelector4.angle = angleSelector5.angle = a;
                                }
                            }
                        }
                    }
                    Column {
                        spacing: 5
                        Label { text: "Disabled" }
                        KisAngleGauge {
                            enabled: false
                            angle: angleGauge.angle
                        }
                    }
                }

                Label { text: "KisAngleSelector"; font.bold: true; }
                Row {
                    spacing: 10
                    leftPadding: 20
                    Column {
                        spacing: 5
                        Label { text: "Enabled" }
                        KisAngleSelector {
                            id: angleSelector1
                            gaugeSize: 40
                        }
                    }
                    Column {
                        spacing: 5
                        Label { text: "Disabled" }
                        KisAngleSelector {
                            id: angleSelector2
                            enabled: false
                        }
                    }
                }
                Row {
                    spacing: 10
                    leftPadding: 20
                    Column {
                        spacing: 5
                        Label { text: "NoFlipOptions" }
                        KisAngleSelector {
                            id: angleSelector3
                            flipOptionsMode: KisAngleSelector.FlipOptionsMode.NoFlipOptions
                            prefix: "The angle: "
                        }
                    }
                    Column {
                        spacing: 5
                        Label { text: "MenuButton" }
                        KisAngleSelector {
                            id: angleSelector4
                            flipOptionsMode: KisAngleSelector.FlipOptionsMode.MenuButton
                        }
                    }
                    Column {
                        spacing: 5
                        Label { text: "ContextMenu" }
                        KisAngleSelector {
                            id: angleSelector5
                            flipOptionsMode: KisAngleSelector.FlipOptionsMode.ContextMenu
                        }
                    }
                    Column {
                        spacing: 5
                        Label { text: "Flat appearance" }
                        KisAngleSelector {
                            id: angleSelector6
                            flipOptionsMode: KisAngleSelector.FlipOptionsMode.ContextMenu
                            flatSpinBox: true
                        }
                    }
                }

                Label { text: "KisCurveWidget"; font.bold: true; }
                Row {
                    spacing: 10;
                    leftPadding: 20
                    Column {
                        spacing: 5
                        Label { text: "Enabled" }
                        KisCurveWidget {
                            id: curveWidget1
                        }
                    }
                    Column {
                        spacing: 5
                        Label { text: "Disabled" }
                        KisCurveWidget {
                            enabled: false
                            curve: curveWidget1.curve
                        }
                    }
                }

            }
        }
    }
}
