cmake_minimum_required(VERSION 3.16)

project(parse_spin_box LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt6 REQUIRED COMPONENTS Core Gui Quick QuickControls2)

add_subdirectory(cpp)
add_subdirectory(qml)

qt_add_executable(
    parse_spin_box
    main.cpp
)

qt_add_qml_module(
    parse_spin_box
    URI /
    NO_RESOURCE_TARGET_PATH
    RESOURCE_PREFIX "/krita/qml"
    QML_FILES
        main.qml
)

target_link_libraries(
    parse_spin_box
    PUBLIC
    Qt::Core
    Qt::Gui
    Qt::Quick
    Qt::QuickControls2
    krita_qml_utilities
    krita_qml_widgets
)
