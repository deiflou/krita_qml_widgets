import QtQuick

Item {
    id: root

    property bool warn: false
    property int warningSignAlignment: Qt.AlignLeft
    property bool showWarningSign: true
    property alias radius: background.radius

    states: [
        State {
            name: "warning"
            when: root.warn && !root.showWarningSign

            PropertyChanges {
                background {
                    opacity: 0.6
                }
                warningSign {
                    opacity: 0
                    x: root.warningSignAlignment == Qt.AlignLeft ? -24 : root.width
                }
            }
        },
        State {
            name: "warningWithSign"
            when: root.warn && root.showWarningSign

            PropertyChanges {
                background {
                    opacity: 0.6
                }
                warningSign {
                    opacity: 1
                    x: root.warningSignAlignment == Qt.AlignLeft ? 0 : root.width - 24
                }
            }
        }
    ]

    transitions: [
        Transition {
            from: ""
            to: "warning"
            PropertyAnimation {
                target: background
                property: "opacity"
                duration: 250
            }
        },
        Transition {
            from: ""
            to: "warningWithSign"
            ParallelAnimation {
                PropertyAnimation {
                    target: background
                    property: "opacity"
                    duration: 250
                }
                PropertyAnimation {
                    target: warningSign
                    properties: "opacity, x"
                    duration: 250
                }
            }
        },
        Transition {
            from: "warning"
            to: ""
            PropertyAnimation {
                target: background
                property: "opacity"
                duration: 250
            }
        },
        Transition {
            from: "warning"
            to: "warningWithSign"
            PropertyAnimation {
                target: warningSign
                properties: "opacity, x"
                duration: 250
            }
        },
        Transition {
            from: "warningWithSign"
            to: ""
            PropertyAnimation {
                target: background
                property: "opacity"
                duration: 250
            }
            PropertyAnimation {
                target: warningSign
                properties: "opacity, x"
                duration: 250
            }
        },
        Transition {
            from: "warningWithSign"
            to: "warning"
            PropertyAnimation {
                target: warningSign
                properties: "opacity, x"
                duration: 250
            }
        }
    ]

    Rectangle {
        id: background
        anchors.fill: parent
        color: Qt.rgba(1, 0.2, 0, 1)
        opacity: 0
    }

    Item {
        id: warningSign
        x: root.warningSignAlignment == Qt.AlignLeft ? -24 : root.width
        anchors.verticalCenter: parent.verticalCenter
        width: 24
        height: 24
        opacity: 0
        
        Image {
            anchors.fill: parent
            anchors.margins: 4
            source: "qrc:/krita/qml/Krita/Resources/images/16_light_warning.svg"
        }
    }
}
