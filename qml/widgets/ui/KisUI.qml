pragma Singleton

import QtQml
import "KisThemePalette.js" as KisThemePalette

QtObject {
    readonly property int toolTipDelay: 1000
    readonly property int toolTipTimeout: 10000

    signal paletteChanged()

    function applyDarkPaletteToWindow(window)
    {
        KisThemePalette.applyDarkPaletteToWindow(window);
        paletteChanged();
    }
    function applyLightPaletteToWindow(window)
    {
        KisThemePalette.applyLightPaletteToWindow(window);
        paletteChanged();
    }
}
