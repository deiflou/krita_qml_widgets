function fixColorsRecursive(obj, window, setPaletteObj)
{
    if (obj.toString().startsWith("KisToolTip") ||
        obj.toString().startsWith("Menu")) {
        setPaletteObj(obj, false);
    } else if (obj && obj !== window.contentItem && obj.palette) {
        obj.palette = window.contentItem.palette;
    }
    if (obj.icon && obj.icon.color) {
        obj.icon.color =
            Qt.binding(
                () => {
                    if ("enabled" in obj && !obj.enabled) {
                        return window.contentItem.palette.disabled.text;
                    } else if (!window.active) {
                        return window.contentItem.palette.inactive.text;
                    }
                    return window.contentItem.palette.active.text;
                }
            );
    }
    
    if (obj.data) {
        for (let c of obj.data) {
            if (c) {
                fixColorsRecursive(c, window, setPaletteObj);
            }
        }
    }
}

function setDarkPaletteObj(obj, fadeInactive = true)
{
    obj.palette.active.window = Qt.rgba(0.18, 0.18, 0.20, 1);
    obj.palette.active.windowText = "white";
    obj.palette.active.base = Qt.rgba(0.125, 0.125, 0.125, 1);
    obj.palette.active.alternateBase = Qt.rgba(0.1875, 0.1875, 0.1875, 1);
    obj.palette.active.toolTipBase = Qt.rgba(0.125, 0.125, 0.125, 1);
    obj.palette.active.toolTipText = "white";
    obj.palette.active.placeholderText = "gray";
    obj.palette.active.text = "white";
    obj.palette.active.button = Qt.rgba(0.18, 0.18, 0.20, 1);
    obj.palette.active.buttonText = "white";
    obj.palette.active.brightText = "white";
    obj.palette.active.light = Qt.rgba(0.5, 0.5, 0.5, 1);
    obj.palette.active.midlight = Qt.rgba(0.375, 0.375, 0.375, 1);
    obj.palette.active.dark = Qt.rgba(0.125, 0.125, 0.125, 1);
    obj.palette.active.mid = Qt.rgba(0.18, 0.18, 0.20, 1);
    obj.palette.active.shadow = "black";
    obj.palette.active.highlight = Qt.rgba(0.25, 0.50, 1, 1);
    obj.palette.active.highlightedText = "black";

    if (fadeInactive) {
        obj.palette.inactive.window = Qt.darker(obj.palette.active.window, 1.4);
        obj.palette.inactive.windowText = Qt.darker(obj.palette.active.windowText, 1.4);
        obj.palette.inactive.base = Qt.darker(obj.palette.active.base, 1.4);
        obj.palette.inactive.alternateBase = Qt.darker(obj.palette.active.alternateBase, 1.4);
        obj.palette.inactive.toolTipBase = Qt.darker(obj.palette.active.toolTipBase, 1.4);
        obj.palette.inactive.toolTipText = Qt.darker(obj.palette.active.toolTipText, 1.4);
        obj.palette.inactive.placeholderText = Qt.darker(obj.palette.active.placeholderText, 1.4);
        obj.palette.inactive.text = Qt.darker(obj.palette.active.text, 1.4);
        obj.palette.inactive.button = Qt.darker(obj.palette.active.button, 1.4);
        obj.palette.inactive.buttonText = Qt.darker(obj.palette.active.buttonText, 1.4);
        obj.palette.inactive.brightText = Qt.darker(obj.palette.active.brightText, 1.4);
        obj.palette.inactive.light = Qt.darker(obj.palette.active.light, 1.4);
        obj.palette.inactive.midlight = Qt.darker(obj.palette.active.midlight, 1.4);
        obj.palette.inactive.dark = Qt.darker(obj.palette.active.dark, 1.4);
        obj.palette.inactive.mid = Qt.darker(obj.palette.active.mid, 1.4);
        obj.palette.inactive.shadow = Qt.darker(obj.palette.active.shadow, 1.4);
        obj.palette.inactive.highlight = Qt.darker(obj.palette.active.highlight, 1.4);
        obj.palette.inactive.highlightedText = Qt.darker(obj.palette.active.highlightedText, 1.4);
    } else {
        obj.palette.inactive.window = obj.palette.active.window;
        obj.palette.inactive.windowText = obj.palette.active.windowText;
        obj.palette.inactive.base = obj.palette.active.base;
        obj.palette.inactive.alternateBase = obj.palette.active.alternateBase;
        obj.palette.inactive.toolTipBase = obj.palette.active.toolTipBase;
        obj.palette.inactive.toolTipText = obj.palette.active.toolTipText;
        obj.palette.inactive.placeholderText = obj.palette.active.placeholderText;
        obj.palette.inactive.text = obj.palette.active.text;
        obj.palette.inactive.button = obj.palette.active.button;
        obj.palette.inactive.buttonText = obj.palette.active.buttonText;
        obj.palette.inactive.brightText = obj.palette.active.brightText;
        obj.palette.inactive.light = obj.palette.active.light;
        obj.palette.inactive.midlight = obj.palette.active.midlight;
        obj.palette.inactive.dark = obj.palette.active.dark;
        obj.palette.inactive.mid = obj.palette.active.mid;
        obj.palette.inactive.shadow = obj.palette.active.shadow;
        obj.palette.inactive.highlight = obj.palette.active.highlight;
        obj.palette.inactive.highlightedText = obj.palette.active.highlightedText;
    }

    obj.palette.disabled.window = Qt.alpha(obj.palette.active.window, 0.5);
    obj.palette.disabled.windowText = Qt.alpha(obj.palette.active.windowText, 0.5);
    obj.palette.disabled.base = Qt.alpha(obj.palette.active.base, 0.5);
    obj.palette.disabled.alternateBase = Qt.alpha(obj.palette.active.alternateBase, 0.5);
    obj.palette.disabled.toolTipBase = Qt.alpha(obj.palette.active.toolTipBase, 0.5);
    obj.palette.disabled.toolTipText = Qt.alpha(obj.palette.active.toolTipText, 0.5);
    obj.palette.disabled.placeholderText = Qt.alpha(obj.palette.active.placeholderText, 0.5);
    obj.palette.disabled.text = Qt.alpha(obj.palette.active.text, 0.5);
    obj.palette.disabled.button = Qt.alpha(obj.palette.active.button, 0.5);
    obj.palette.disabled.buttonText = Qt.alpha(obj.palette.active.buttonText, 0.5);
    obj.palette.disabled.brightText = Qt.alpha(obj.palette.active.brightText, 0.5);
    obj.palette.disabled.light = Qt.alpha(obj.palette.active.light, 0.5);
    obj.palette.disabled.midlight = Qt.alpha(obj.palette.active.midlight, 0.5);
    obj.palette.disabled.dark = Qt.alpha(obj.palette.active.dark, 0.5);
    obj.palette.disabled.mid = Qt.alpha(obj.palette.active.mid, 0.5);
    obj.palette.disabled.shadow = Qt.alpha(obj.palette.active.shadow, 0.5);
    obj.palette.disabled.highlight = Qt.alpha(obj.palette.active.highlight, 0.5);
    obj.palette.disabled.highlightedText = Qt.alpha(obj.palette.active.highlightedText, 0.5);
}

function setLightPaletteObj(obj, fadeInactive = true)
{
    obj.palette.active.window = Qt.rgba(0.9, 0.9, 1, 1);
    obj.palette.active.windowText = "black";
    obj.palette.active.base = Qt.rgba(0.98, 0.98, 1, 1);
    obj.palette.active.alternateBase = Qt.rgba(0.95, 0.95, 1, 1);
    obj.palette.active.toolTipBase = Qt.rgba(0.98, 0.98, 1, 1);
    obj.palette.active.toolTipText = "black";
    obj.palette.active.placeholderText = "gray";
    obj.palette.active.text = "black";
    obj.palette.active.button = Qt.rgba(0.9, 0.9, 1, 1);
    obj.palette.active.buttonText = "black";
    obj.palette.active.brightText = "white";
    obj.palette.active.light = Qt.rgba(0.9, 0.9, 1, 1);
    obj.palette.active.midlight = Qt.rgba(0.85, 0.85, 0.85, 1);
    obj.palette.active.dark = Qt.rgba(0.5, 0.5, 0.5, 1);
    obj.palette.active.mid = Qt.rgba(0.75, 0.75, 0.75, 1);
    obj.palette.active.shadow = "black";
    obj.palette.active.highlight = Qt.rgba(0.25, 0.50, 1, 1);
    obj.palette.active.highlightedText = "black";

    if (fadeInactive) {
        obj.palette.inactive.window = Qt.darker(obj.palette.active.window, 1.1);
        obj.palette.inactive.windowText = Qt.darker(obj.palette.active.windowText, 1.1);
        obj.palette.inactive.base = Qt.darker(obj.palette.active.base, 1.1);
        obj.palette.inactive.alternateBase = Qt.darker(obj.palette.active.alternateBase, 1.1);
        obj.palette.inactive.toolTipBase = Qt.darker(obj.palette.active.toolTipBase, 1.1);
        obj.palette.inactive.toolTipText = Qt.darker(obj.palette.active.toolTipText, 1.1);
        obj.palette.inactive.placeholderText = Qt.darker(obj.palette.active.placeholderText, 1.1);
        obj.palette.inactive.text = Qt.darker(obj.palette.active.text, 1.1);
        obj.palette.inactive.button = Qt.darker(obj.palette.active.button, 1.1);
        obj.palette.inactive.buttonText = Qt.darker(obj.palette.active.buttonText, 1.1);
        obj.palette.inactive.brightText = Qt.darker(obj.palette.active.brightText, 1.1);
        obj.palette.inactive.light = Qt.darker(obj.palette.active.light, 1.1);
        obj.palette.inactive.midlight = Qt.darker(obj.palette.active.midlight, 1.1);
        obj.palette.inactive.dark = Qt.darker(obj.palette.active.dark, 1.1);
        obj.palette.inactive.mid = Qt.darker(obj.palette.active.mid, 1.1);
        obj.palette.inactive.shadow = Qt.darker(obj.palette.active.shadow, 1.1);
        obj.palette.inactive.highlight = Qt.darker(obj.palette.active.highlight, 1.1);
        obj.palette.inactive.highlightedText = Qt.darker(obj.palette.active.highlightedText, 1.1);
    } else {
        obj.palette.inactive.window = obj.palette.active.window;
        obj.palette.inactive.windowText = obj.palette.active.windowText;
        obj.palette.inactive.base = obj.palette.active.base;
        obj.palette.inactive.alternateBase = obj.palette.active.alternateBase;
        obj.palette.inactive.toolTipBase = obj.palette.active.toolTipBase;
        obj.palette.inactive.toolTipText = obj.palette.active.toolTipText;
        obj.palette.inactive.placeholderText = obj.palette.active.placeholderText;
        obj.palette.inactive.text = obj.palette.active.text;
        obj.palette.inactive.button = obj.palette.active.button;
        obj.palette.inactive.buttonText = obj.palette.active.buttonText;
        obj.palette.inactive.brightText = obj.palette.active.brightText;
        obj.palette.inactive.light = obj.palette.active.light;
        obj.palette.inactive.midlight = obj.palette.active.midlight;
        obj.palette.inactive.dark = obj.palette.active.dark;
        obj.palette.inactive.mid = obj.palette.active.mid;
        obj.palette.inactive.shadow = obj.palette.active.shadow;
        obj.palette.inactive.highlight = obj.palette.active.highlight;
        obj.palette.inactive.highlightedText = obj.palette.active.highlightedText;
    }

    obj.palette.disabled.window = Qt.alpha(obj.palette.active.window, 0.5);
    obj.palette.disabled.windowText = Qt.alpha(obj.palette.active.windowText, 0.5);
    obj.palette.disabled.base = Qt.alpha(obj.palette.active.base, 0.5);
    obj.palette.disabled.alternateBase = Qt.alpha(obj.palette.active.alternateBase, 0.5);
    obj.palette.disabled.toolTipBase = Qt.alpha(obj.palette.active.toolTipBase, 0.5);
    obj.palette.disabled.toolTipText = Qt.alpha(obj.palette.active.toolTipText, 0.5);
    obj.palette.disabled.placeholderText = Qt.alpha(obj.palette.active.placeholderText, 0.5);
    obj.palette.disabled.text = Qt.alpha(obj.palette.active.text, 0.5);
    obj.palette.disabled.button = Qt.alpha(obj.palette.active.button, 0.5);
    obj.palette.disabled.buttonText = Qt.alpha(obj.palette.active.buttonText, 0.5);
    obj.palette.disabled.brightText = Qt.alpha(obj.palette.active.brightText, 0.5);
    obj.palette.disabled.light = Qt.alpha(obj.palette.active.light, 0.5);
    obj.palette.disabled.midlight = Qt.alpha(obj.palette.active.midlight, 0.5);
    obj.palette.disabled.dark = Qt.alpha(obj.palette.active.dark, 0.5);
    obj.palette.disabled.mid = Qt.alpha(obj.palette.active.mid, 0.5);
    obj.palette.disabled.shadow = Qt.alpha(obj.palette.active.shadow, 0.5);
    obj.palette.disabled.highlight = Qt.alpha(obj.palette.active.highlight, 0.5);
    obj.palette.disabled.highlightedText = Qt.alpha(obj.palette.active.highlightedText, 0.5);
}

function applyDarkPaletteToWindow(window)
{
    setDarkPaletteObj(window.contentItem);
    fixColorsRecursive(window.contentItem, window, setDarkPaletteObj);
    window.color = Qt.binding(() => { return window.active ? window.contentItem.palette.active.window : window.contentItem.palette.inactive.window; });
}

function applyLightPaletteToWindow(window)
{
    setLightPaletteObj(window.contentItem);
    fixColorsRecursive(window.contentItem, window, setLightPaletteObj);
    window.color = Qt.binding(() => { return window.active ? window.contentItem.palette.active.window : window.contentItem.palette.inactive.window; });
}
