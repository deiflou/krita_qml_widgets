import QtQuick
import "../tooltip"

Item {
    id: root

    property bool softRangeActive: false
    property color color: "black"
    property alias toolTip: toolTipPopup.text

    opacity: 0.4

    states: [
        State {
            name: "hovered"
            when: root.enabled && !root.softRangeActive && mouseArea.containsMouse

            PropertyChanges {
                root {
                    opacity: 1
                }
            }
        },
        State {
            name: "softRangeActive"
            when: root.softRangeActive && (!root.enabled || !mouseArea.containsMouse)

            PropertyChanges {
                rangeSwitchOuterCircle {
                    opacity: 1
                }

                rangeSwitchInnerCircle {
                    width: rangeSwitchOuterCircle.width * 0.5 - 1
                }
            }
        },
        State {
            name: "softRangeActiveAndHovered"
            when: root.enabled && root.softRangeActive && mouseArea.containsMouse

            PropertyChanges {
                rangeSwitchOuterCircle {
                    opacity: 1
                }

                rangeSwitchInnerCircle {
                    width: rangeSwitchOuterCircle.width * 0.5 - 1
                }

                root {
                    opacity: 1
                }
            }
        }
    ]

    transitions: [
        Transition {
            from: "*"
            to: "*"
            PropertyAnimation {
                target: root
                property: "opacity"
                duration: 150
            }
            PropertyAnimation {
                target: rangeSwitchOuterCircle
                property: "opacity"
                duration: 150
            }
            PropertyAnimation {
                target: rangeSwitchInnerCircle
                property: "width"
                duration: 150
            }
        }
    ]

    Rectangle {
        id: rangeSwitchOuterCircle

        width: 10
        height: width
        x: (parent.width - width) * 0.5
        y: (parent.height - height) * 0.5
        radius: width * 0.5
        border.width: 1
        border.color: root.color
        color: "transparent"
        opacity: 0
    }

    Rectangle {
        id: rangeSwitchInnerCircle

        width: rangeSwitchOuterCircle.width
        height: width
        x: (parent.width - width) * 0.5
        y: (parent.height - height) * 0.5
        radius: width * 0.5
        color: root.color
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true

        onClicked: root.softRangeActive = !root.softRangeActive
    }

    KisToolTip {
        id: toolTipPopup
        parent: root
        visible: root.enabled && mouseArea.containsMouse
    }
}
