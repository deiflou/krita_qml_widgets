set_source_files_properties(
    ui/KisUI.qml
    PROPERTIES
    QT_QML_SINGLETON_TYPE TRUE
)

qt_add_qml_module(
    krita_qml_widgets
    URI Krita.Widgets
    VERSION 1.0
    SHARED
    OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/Krita/Widgets"
    RESOURCE_PREFIX "/krita/qml"
    QML_FILES
        button/KisGroupButton.qml
        button/KisOptionButtonStrip.qml
        
        ui/KisUI.qml
        ui/KisThemePalette.js

        tooltip/KisToolTip.qml
        
        overlays/KisWarningOverlay.qml
        overlays/KisSliderOverlay.qml

        textinput/KisTextInputWithSelectionRange.qml
        textinput/KisTextInputWithPrefixAndSuffix.qml

        spinbox/KisParseSpinBoxContentItem.qml
        spinbox/KisIntParseSpinBox.qml
        spinbox/KisDoubleSpinBox.qml
        spinbox/KisDoubleParseSpinBox.qml
        spinbox/KisParseSpinBoxUtil.js

        sliderspinbox/KisSliderSpinBoxManipulator.qml
        sliderspinbox/KisSliderSpinBoxRangeSwitch.qml
        sliderspinbox/KisSliderSpinBoxContentItem.qml
        sliderspinbox/KisIntSliderSpinBox.qml
        sliderspinbox/KisDoubleSliderSpinBox.qml

        angleselector/KisAngleGauge.qml
        angleselector/KisAngleSelectorUtil.js
        angleselector/KisAngleSelector.qml

        curvewidget/KisCurveWidget.qml
)

target_link_libraries(
    krita_qml_widgets
    PUBLIC
    Qt::Quick
    Qt::QuickControls2
    krita_qml_utilities
    krita_qml_resources
    krita_qml_types
)
