import QtQuick
import QtQuick.Controls
import "../ui"

ToolTip {
    id: root

    delay: KisUI.toolTipDelay
    timeout: KisUI.toolTipTimeout

    property Item parentControl
    property bool changingParentControl: false

    onParentChanged: {
        if (changingParentControl) {
            return;
        }
        if (parentControl) {
            parentControl = undefined;
            root.visible = false;
        }
    }

    onParentControlChanged: {
        changingParentControl = true;
        if (root.parentControl) {
            root.parent = root.parentControl;
            root.parentControl.hoverEnabled = true;
            root.visible = Qt.binding(() => { return root.parentControl.hovered });
        } else {
            root.parent = undefined;
            root.visible = false;
        }
        changingParentControl = false;
    }
}
