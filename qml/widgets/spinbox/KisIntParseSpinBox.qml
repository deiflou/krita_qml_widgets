import QtQuick
import QtQuick.Controls

SpinBox {
    id: root

    property alias prefix: contentId.prefix
    property alias suffix: contentId.suffix

    editable: true
    padding: 0
    focusPolicy: Qt.WheelFocus
    implicitWidth: contentId.implicitWidth + 1 + 2 + leftPadding + rightPadding

    contentItem: FocusScope {
        KisParseSpinBoxContentItem {
            id: contentId

            anchors.fill: parent
            anchors.margins: 1
            anchors.rightMargin: 2
            focus: true
     
            value: root.value
            from: root.from
            to: root.to
            parentSpinBox: root

            onValueChanged: root.value = Math.round(value)
        }
    }

    onValueChanged: contentId.value = root.value
}
