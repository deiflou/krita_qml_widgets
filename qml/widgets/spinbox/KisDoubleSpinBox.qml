import QtQuick
import QtQuick.Controls
import "KisParseSpinBoxUtil.js" as KisParseSpinBoxUtil

SpinBox {
    id: root

    property int decimals: 2
    property real dValue: 0.0
    property real dFrom: 0.0
    property real dTo: 100.0
    property real dStepSize: 1.0

    property real factor: Math.pow(10, decimals)

    stepSize: Math.round(dStepSize * factor)
    from: Math.round(dFrom * factor)
    to: Math.round(dTo * factor)

    onValueChanged: root.dValue = root.value / root.factor;

    onDecimalsChanged: root.dValue = KisParseSpinBoxUtil.truncateValue(root.dValue, root.decimals)

    onDValueChanged: {
        let v = root.dValue;
        if (v > root.dTo) {
            v = root.dTo;
        } else if (v < root.dFrom) {
            v = root.dFrom;
        }
        v = KisParseSpinBoxUtil.truncateValue(v, root.decimals);
        if (v !== root.dValue) {
            root.dValue = v;
        }
        root.value = Math.round(root.dValue * root.factor);
    }

    onDFromChanged: {
        if (root.dFrom > root.dTo) {
            root.dFrom = root.dTo;
        }
        if (root.dFrom > root.dValue) {
            root.dValue = KisParseSpinBoxUtil.truncateValue(root.dFrom, root.decimals);
        }
    }

    onDToChanged: {
        if (root.dTo < root.dFrom) {
            root.dTo = root.dFrom;
        }
        if (root.dTo < root.dValue) {
            root.dValue = KisParseSpinBoxUtil.truncateValue(root.dTo, root.decimals);
        }
    }

    Component.onCompleted: root.value = Math.round(root.dValue * root.factor)
}
