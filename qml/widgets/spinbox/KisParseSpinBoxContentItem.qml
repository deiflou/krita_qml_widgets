import QtQml
import QtQuick
import QtQuick.Controls
import Krita.Utilities
import "../overlays"
import "../textinput"
import "KisParseSpinBoxUtil.js" as KisParseSpinBoxUtil

FocusScope {
    id: root

    property real value: 0.0
    property int decimals: 0
    property real from: 0
    property real to: 0
    property alias prefix: textInput.prefix
    property alias suffix: textInput.suffix
    property alias warningOverlay: warningOverlay
    property alias textInput: textInput
    required property SpinBox parentSpinBox

    signal editingFinished()
    signal editingCanceled()
    signal valueParsed(real parsedValue)

    implicitHeight: textInput.implicitHeight
    implicitWidth: textMetrics.boundingRect.width + textInput.padding * 2
    
    onValueChanged: textInput.contentsText = KisParseSpinBoxUtil.truncateValue(root.value, root.decimals)

    onDecimalsChanged: textInput.contentsText = KisParseSpinBoxUtil.truncateValue(root.value, root.decimals)

    KisWarningOverlay {
        id: warningOverlay
        anchors.fill: parent
        clip: true

        radius: 1
        showWarningSign: width - textInput.displayTextWidth > 24 * (textInput.horizontalAlignment === Qt.AlignHCenter ? 2 : 1)
        warningSignAlignment: Qt.AlignRight
    }

    KisTextInputWithPrefixAndSuffix {
        id: textInput

        property real parsedValue: 0.0

        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        padding: 4
        contentsText: KisParseSpinBoxUtil.truncateValue(root.value, root.decimals)
        focus: true
        font: root.parentSpinBox.font
        color: root.parentSpinBox.palette.text
        selectionColor: root.parentSpinBox.palette.highlight
        selectedTextColor: root.parentSpinBox.palette.highlightedText
        inputMethodHints: root.parentSpinBox.inputMethodHints
        readOnly: !root.parentSpinBox.editable

        onEditingFinished: {
            warningTimer.stop();
            if (isNaN(parsedValue)) {
                warningOverlay.warn = true;
            } else {
                warningOverlay.warn = false;
                root.value = KisParseSpinBoxUtil.truncateValue(parsedValue, root.decimals);
                root.editingFinished();
            }
        }

        onContentsTextChanged: {
            parsedValue = KisNumParser.parseSimpleMathExpr(contentsText);
            warningOverlay.warn = false;
            if (isNaN(parsedValue)) {
                warningTimer.restart();
            } else {
                warningTimer.stop();
            }
            root.valueParsed(parsedValue)
        }

        Keys.onEscapePressed: {
            warningTimer.stop();
            warningOverlay.warn = false;
            textInput.contentsText = KisParseSpinBoxUtil.truncateValue(root.value, root.decimals);
            root.editingCanceled();
        }

        Timer {
            id: warningTimer
            interval: 2000;
            onTriggered: warningOverlay.warn = true
        }

        MouseArea {
            property real cumulatedTrackpadLength: 0.0

            anchors.fill: parent
            acceptedButtons: Qt.NoButton
            cursorShape: textInput.readOnly ? Qt.ArrowCursor : Qt.IBeamCursor

            onWheel: (we) => {
                if (root.parentSpinBox.focusPolicy & Qt.WheelFocus) {
                    if (!textInput.activeFocus) {
                        textInput.forceActiveFocus(Qt.MouseFocusReason);
                        textInput.selectAll();
                    }
                }

                let inc = 0.0
                if (we.pixelDelta && we.pixelDelta.y !== 0) {
                    cumulatedTrackpadLength += we.pixelDelta.y;
                    if (Math.abs(cumulatedTrackpadLength) > 30) {
                        inc = cumulatedTrackpadLength;
                        cumulatedTrackpadLength = 0.0;
                    }
                } else {
                    inc = we.angleDelta.y;
                }

                inc *= we.inverted ? -1 : 1; 
                if (inc > 0) {
                    root.parentSpinBox.increase();
                    textInput.selectAll();
                } else if (inc < 0) {
                    root.parentSpinBox.decrease();
                    textInput.selectAll();
                }
            }
        }
    }

    TextMetrics {
        id: textMetrics
        font: textInput.font
        text: {
            const fromText = KisParseSpinBoxUtil.truncateValue(root.from, root.decimals);
            const toText = KisParseSpinBoxUtil.truncateValue(root.to, root.decimals);
            return root.prefix
                    + (fromText.length > toText.length ? fromText : toText)
                    + root.suffix
        }
    }
}
