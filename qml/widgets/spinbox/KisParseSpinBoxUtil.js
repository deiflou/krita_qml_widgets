function truncateValue(value, decimals)
{
    const reg = new RegExp("^-?\\d+(?:\\.\\d{0," + decimals + "})?", "g");
    const a = value.toString().match(reg)[0];
    const dot = a.indexOf(".");
    if (dot === -1) {
        if (decimals < 1) {
            return a;
        }
        return a + "." + "0".repeat(decimals);
    }
    if (decimals < 1) {
        return a.slice(0, dot);
    }
    const b = decimals - (a.length - dot) + 1;
    return b > 0 ? (a + "0".repeat(b)) : a;
}
