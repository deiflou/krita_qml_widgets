#ifndef KISNUMPARSER_H
#define KISNUMPARSER_H

#include <QObject>
#include <QString>
#include <QQmlEngine>

#include <limits>

#include <kis_num_parser.h>

class KisNumParser : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public:
    //! \brief parse an expression to a double.
    Q_INVOKABLE double parseSimpleMathExpr(QString const &expr) const;
};

#endif
