#include "KisNumParser.h"

double KisNumParser::parseSimpleMathExpr(QString const &expr) const
{
    bool ok = false;
    const double ret = KisNumericParser::parseSimpleMathExpr(expr, &ok);
    if (!ok) {
        return std::numeric_limits<double>::quiet_NaN();
    }
    return ret;
}
